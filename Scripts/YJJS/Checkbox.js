﻿/** 
 *@Name：CheckBox.js
 *@Author：Yongjin.C 995292291@qq.com
 */
$.fn.CheckBoxBindFn = function(op) {
    var option = {
        Allclass: "all",
        itemclass: "item",
        callback: function($item) { return false; }
    };
    $.extend(option, op || {});
    var itemstr = "input[type=checkbox]" + (!!option.itemclass ? "[class=" + option.itemclass + "]" : "");
    var $checkbox = $(this).find(itemstr);
    var $all = $checkbox.filter("." + option.Allclass);
    $checkbox.change(function() {
        var $item = $(this);
        if ($item.prop("checked")) {
            $item.attr("checked", true);
        } else {
            $item.attr("checked", false);
        }
        if ($item.hasClass(option.Allclass)) {
            if ($item.prop("checked")) {
                $checkbox.prop("checked", true).attr("checked", true);;
            } else {
                $checkbox.prop("checked", false).attr("checked", false);
            }
        } else {
            if (!$item.prop("checked")) {
                $all.prop("checked", false).attr("checked", false);
            }
        }
        option.callback($item);
    });
}

$.CheckBox = {
    option: {
        All: "input[type=checkbox].all",
        item: "input[type=checkbox].item"
    },
    onclick: function(obj, parentDom) {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj">input[type=checkbox] node </param>
        /// <param name="parentDom">.class/#id</param>
        var sel = parentDom || "";
        sel += " ";
        var $item = $(obj);
        if ($item.prop("checked")) {
            $item.attr("checked", true);
        } else {
            $item.attr("checked", false);
        }
        if ($item.hasClass("all")) {
            if ($item.prop("checked")) {
                $(sel + this.option.item).prop("checked", true).attr("checked", true); //attribute和property 
            } else {
                $(sel + this.option.item).prop("checked", false).attr("checked", false);
            }
        } else {
            if (!$item.prop("checked")) {
                $(sel + this.option.All).prop("checked", false).attr("checked", false);
            }
        }
    }

}