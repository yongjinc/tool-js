﻿/** 
 *@Name：HashTable,HashMap,StringBuilder 扩展
 *@Author：Yongjin.C 995292291@qq.com
 */
function HashTable() {
    var _hash = {};
    this.add = function(key, value) {
        if (typeof (key) != "undefined") {
            if (this.contains(key) == false) {
                _hash[key] = typeof (value) == "undefined" ? null : value;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    this.remove = function(key) {
        delete _hash[key];
    }
    this.count = function() {
        var i = 0;
        for (var k in _hash) {
            if (_hash.hasOwnProperty(k)) {
                i++;
            }
        }
        return i;
    }
    this.items = function(key) {
        return _hash[key];
    }
    this.contains = function(key) {
        return typeof (_hash[key]) != "undefined";
    }
    this.clear = function() {
        // for (var k in this._hash) { delete this._hash[k]; }
        _hash = {};
    }
    this.getHash = function() {
        return _hash;
    }
}

function HashMap() {
    //定义长度
    var length = 0;
    //创建一个对象
    var _obj = {};

    /**
    * 判断Map是否为空
    */
    this.isEmpty = function() {
        return length == 0;
    };
    this.getHash = function() {
        return _obj;
    }

    /**
    * 判断对象中是否包含给定Key
    */
    this.containsKey = function(key) {
        // return (key in _obj);
        return _obj.hasOwnProperty(key);
    };

    /**
    * 判断对象中是否包含给定的Value
    */
    this.containsValue = function(value) {
        for (var key in _obj) {
            if (_obj[key] == value) {
                return true;
            }
        }
        return false;
    };
    this.contains = function(key) {
        return typeof (_obj[key]) != "undefined";
    }
    /**
    *向map中添加数据
    */
    this.put = function(key, value) {
        if (typeof (key) != "undefined") {
            if (this.containsKey(key) == false) {
                length++;
            }
            _obj[key] = typeof (value) == "undefined" ? null : value;
            return true;
        }
        return false;
    };

    /**
    * 根据给定的Key获得Value
    */
    this.get = function(key) {
        return this.containsKey(key) ? _obj[key] : null;
    };

    /**
    * 根据给定的Key删除一个值
    */
    this.remove = function(key) {
        if (this.containsKey(key) && (delete _obj[key])) {
            length--;
        }
    };

    /**
    * 获得Map中的所有Value
    */
    this.values = function() {
        var _values = new Array();
        for (var key in _obj) {
            _values.push(_obj[key]);
        }
        return _values;
    };

    /**
    * 获得Map中的所有Key
    */
    this.keySet = function() {
        var _keys = new Array();
        for (var key in _obj) {
            _keys.push(key);
        }
        return _keys;
    };

    /**
    * 获得Map的长度
    */
    this.size = function() {
        return length;
    };

    /**
    * 清空Map
    */
    this.clear = function() {
        length = 0;
        _obj = {};
    };
}

function StringBuilder() {
    var sb = [];
    var _length = 0;
    if (arguments[0] != undefined && arguments[0] != null) {
        sb[0] = arguments[0];
    }
    this.append = function(str) {
        _length += str.length;
        //  sb[sb.length] = str;
        sb.push(str);
    };
    this.appendFormat = function() {
        if (arguments.length > 1) {
            var TString = arguments[0];
            if (arguments[1] instanceof Array) {
                for (var i = 0,
                    iLen = arguments[1].length; i < iLen; i++) {
                    var jIndex = i;
                    var re = eval("/\\{" + jIndex + "\\}/g;");
                    TString = TString.replace(re, arguments[1][i]);
                }
            } else {
                for (var i = 1,
                    iLen = arguments.length; i < iLen; i++) {
                    var jIndex = i - 1;
                    var re = eval("/\\{" + jIndex + "\\}/g;");
                    TString = TString.replace(re, arguments[i]);
                }
            }
            this.Append(TString);
        } else if (arguments.length == 1) {
            this.Append(arguments[0]);
        }
    };
    this.toString = function() {
        if (arguments[0] != undefined && arguments[0] != null) {
            return sb.join(arguments[0]);
        }
        return sb.join("");
    };
    this.replace = function(index, str) {
        _length = _length - sb[index].length + str.length;
        sb.splice(index, 1, str);
        // sb[index] = str;
    };
    this.length = function() {
        return _length;
    }
    this.Clear = function() {
        sb = [];
        _length = 0;
    }
    this.IsEmpty = function() {
        return sb.length <= 0;
    }
    this.remove = function(index) {
        _length -= sb[index].length;
        sb.splice(index, 1);
    };
    this.insert = function(index, str) {
        _length += str.length;
        sb.splice(index, 0, str);
    };
};