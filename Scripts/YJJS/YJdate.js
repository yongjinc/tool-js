﻿/** 
 *@Name：Date 操作 扩展
 *@Author：Yongjin.C 995292291@qq.com
 */
;!function(window, undefined) {
    var _Yj = window.YJTool || {};
    _Yj.Date = {
        getDate: function(strDate) {
            /// <summary>
            /// strDate:style "yyyy-MM-dd"|"yyyy/MM/dd"年y,月M,日d,时h,分m,秒s
            /// </summary>
            /// <param name="strDate"></param>
            /// <returns type=""></returns>
            strDate = strDate.replace(/\d+(?=[-\/][^-\/]+$)/, function(a) { return parseInt(a, 10) - 1; });
            var date = eval('new Date(' + strDate.match(/\d+/g) + ')');
            return date;
        },
        DateToString: function(datetime, style) {
            // datetime = datetime || new Date();
            var o = {
                "M+": datetime.getMonth() + 1, //month
                "d+": datetime.getDate(), //day
                "h+": datetime.getHours(), //hour
                "m+": datetime.getMinutes(), //minute
                "s+": datetime.getSeconds(), //second
                "w+": "\u65e5\u4e00\u4e8c\u4e09\u56db\u4e94\u516d".charAt(datetime.getDay()), //week
                "q+": Math.floor((datetime.getMonth() + 3) / 3), //quarter
                "S": datetime.getMilliseconds() //millisecond
            };
            if (/(y+)/.test(style)) {
                style = style.replace(RegExp.$1, (datetime.getFullYear() + "").substr(4 - RegExp.$1.length));
            }
            for (var k in o) {
                if (new RegExp("(" + k + ")").test(style)) {
                    style = style.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
                }
            }
            return style;
        },
        /**获取今天**/
        //  getToday: function() {
        //      return new Date();
        //  },
        /**获取前几天**/
        getPreDays: function(days) {
            var date = new Date();
            date.setDate(date.getDate() - days);
            return date;
        },
        /**获取前几月**/
        getPreMonths: function(months) {
            var date = new Date();
            date.setMonth(date.getMonth() - months);
            return date;
        },
        /*获取某年某月天数*/
        getMonthDays: function(year, month) {
            //js 月份 0-11,而下个月第0天为上个月天数或上个月最后一天
            /*
            *===>new Date(2014,0,0)
            *===>Tue Dec 31 2013 00:00:00 GMT+0800 (中国标准时间)*/
            return new Date(year, month, 0).getDate();
        },
        getYearDays: function(year) {
            return isLeapYear(year) ? 356 : 355;
        },
        isLeapYear: function(year) { //29天
            var Year = year || new Date().getFullYear();
            return ((Year % 4) == 0) && ((Year % 100) != 0) || ((Year % 400) == 0);
        },
        isLeapYear2: function(year) {
            var Year = year || new Date().getFullYear();
            return getMonthDays(Year, 2) == 29;
        },
        getWeaks: function(year) {
            var Year = year || new Date().getFullYear();
            //js 月份 0 为一月
            //星期一为第一天
            var day = new Date(Year, 0, 1).getDay();
            if (day == 0) //周末
                day = 7;
            var days = getYearDays(Year);
            var newdays = days + day - 1;
            var weaks = Math.ceil(newdays / 7); //不管最后余几天也算一周
            // var mod = newdays % 7;
            return weaks;
        },
        getCurWeakStartDay: function() {
            var date = new Date();
            var day = date.getDay();
            date.setDate(date.getDate() - day + 1);
            return date;
        },
        getCurWeakEnfDay: function() {
            var date = new Date();
            var day = date.getDay();
            date.setDate(date.getDate() + 7 - day);
            return date;
        },
        NewDate: function(str) {
            /// <summary>
            /// new date() ,兼容火狐
            /// </summary>
            /// <param name="str"></param>
            /// <returns type=""></returns>
            str = str.split('-');
            var date = new Date();
            date.setUTCFullYear(str[0], str[1] - 1, str[2]);
            date.setUTCHours(0, 0, 0, 0);
            return date;
        }

    }
    window.YJTool = _Yj;

}(window, undefined);