﻿/** 
 *@Name：jquery %.ajax 扩展
 *@Author：Yongjin.C 995292291@qq.com
 */
;
(function($) {
    $.postWebService = function(url, data, fnSuccess) {
        /// <summary>ajax 异步调取 web服务</summary> 
        /// <param name="url" type="String">url</param>
        /// <param name="data"  type="json"> json 对象</param>
        /// <param name="fnSuccess"></param>
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: url,
            data: JSON.stringify(data), //序列化为json格式的字符串
            dataType: "json",
            success: fnSuccess
        });
    }
    $.postWebServiceNoAsync = function(url, data, fnSuccess) {
        /// <summary>ajax 同步调取 web服务</summary>
        /// <param name="url" type="String">url</param>
        /// <param name="data" type="json">json 对象</param>
        /// <param name="fnSuccess"></param>
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: url,
            data: JSON.stringify(data), //序列化为json格式的字符串
            dataType: "json",
            async: false,
            success: fnSuccess
        });
    }
    $.postActionPage = function(url, paramsJson) {
        /// <summary>
        /// post 跳转页面
        /// </summary>
        /// <param name="url" type="String">url</param>
        /// <param name="paramsJson" type="json">参数,json对象</param>
        var form = $("<form method='post'></form>");
        form.attr({ "action": url });
        for (var arg in paramsJson) {
            var input = $("<input type='hidden'>");
            input.attr({ "name": arg });
            input.val(paramsJson[arg]);
            form.append(input);
        }
        $("body").append(form); //不加这局ie下form内容未定义
        form.submit().remove();
    }
    $.getActionPage = function(url, paramsJson) {
        /// <summary>
        /// get 跳转页面
        /// </summary>
        /// <param name="url" type="String">url</param>
        /// <param name="paramsJson" type="json">参数,json对象</param>
        var b = url.search(/\?/);
        if (b == -1)
            self.location.href = url + "?" + $.param(paramsJson);
        else {
            self.location.href = url + "&" + $.param(paramsJson);
        }
    }
    $.StandardPost = $.postActionPage;
    $.StandardGet = $.getActionPage;
    /**
    * jq 扩展
    */
    $.fn.changeValue = function(fn) {
        var change = !+[1,] ? "propertychange" : "input";
        $(this).off(change).on(change, function(e) {
            e.preventDefault();
            fn();
        });
    }
    $.log = $.fn.log = function(msg, isObj) {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="isObj">是否打印源</param>
        if (console) {
            isObj ? console.log({ log: msg, obj: this != $ ? this : { "jQuery": $.fn.jquery } }) :
                console.log({ log: msg });
        } else if (alert) {
            alert(msg.toString());
        }
        return this;
    };
})(jQuery);