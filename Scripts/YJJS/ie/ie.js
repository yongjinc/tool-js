﻿/** 
 *@Name：ie9以下 兼容
 *@Author：Yongjin.C 995292291@qq.com
 */

if (!+[1,]) {
    var isIE = {};
    isIE.getPath = function() {
        var js = document.scripts, jsPath = js[js.length - 1].src;
        return jsPath.substring(0, jsPath.lastIndexOf("/") + 1);
    }
    isIE.addScript = function(model) {
        document.write("<scri" + "pt src='" + isIE.getPath() + model + "'></sc" + "ript>");
    }
    if (!window.JSON || window.JSON.IE8 == undefined) //不管ie8 都重载或定义stringify;
        isIE.addScript("json2.js");
    isIE.addScript("html5shiv.min.js");
    isIE.addScript("respond.min.js");
    isIE.addScript("IE9.js");
    isIE.addScript("ieBetter.js");
    if (!jQuery)
        isIE.addScript("sizzle.js");
    else {
        Sizzle = jQuery.find;
    }

}